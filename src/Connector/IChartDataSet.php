<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 1:07 PM
 */

namespace Smorken\Chart\Connector;


interface IChartDataSet {

    public function setLabel($label);

    public function getLabel();

    public function setData($data);

    public function getData();

    public function addData($data);

    public function getProperties();

    public function setProperties($properties);

    public function getProperty($key);

    public function setProperty($key, $value);

    public function setColors($r, $g, $b);

    public function setColor($property, $r, $g, $b, $a);

    public function toArray();

} 