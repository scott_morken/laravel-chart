<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 3:20 PM
 */

namespace Smorken\Chart\Connector\Dummy;


use Smorken\Chart\Connector\AbstractChartDataSet;
use Smorken\Chart\Connector\IChartDataSet;

class ChartDataSet extends AbstractChartDataSet implements IChartDataSet {

    protected $properties = array(
        'label' => null,
        'fillColor' => null,
        'strokeColor' => null,
        'highlightFill' => null,
        'highlightStroke' => null,
    );

    public function setColors($r, $g, $b)
    {
        $c = array('fillColor' => .5, 'strokeColor' => .8, 'highlightFill' => 0.75, 'highlightStroke' => 1);
        foreach ($c as $which => $val) {
            $this->setColor($which, $r, $g, $b, $val);
        }
    }

}