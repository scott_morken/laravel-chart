<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 3:21 PM
 */

namespace Smorken\Chart\Connector\Dummy;


use Smorken\Chart\Connector\AbstractChartOptions;
use Smorken\Chart\Connector\IChartOptions;

class ChartOptions extends AbstractChartOptions implements IChartOptions {

    protected $options = array(
        'fiz' => 'bat'
    );
} 