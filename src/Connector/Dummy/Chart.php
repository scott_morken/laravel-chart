<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 3:19 PM
 */

namespace Smorken\Chart\Connector\Dummy;


use Smorken\Chart\Connector\AbstractChart;
use Smorken\Chart\Connector\IChart;

class Chart extends AbstractChart implements IChart {

} 