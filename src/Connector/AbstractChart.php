<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 1:05 PM
 */

namespace Smorken\Chart\Connector;


abstract class AbstractChart {

    protected $id;
    /**
     * @var IChartDataSet[]
     */
    protected $chartDataSets;
    /**
     * @var IChartOptions
     */
    protected $chartOptions;

    protected $labels = array();

    protected $properties = array();

    protected $type = 'line';

    /**
     * @param $id
     * @param array $labels
     * @param IChartDataSet[] $chartDataSets
     * @param IChartOptions $chartOptions
     */
    public function __construct($id, array $labels, array $chartDataSets, IChartOptions $chartOptions)
    {
        $this->setId($id);
        $this->setLabels($labels);
        $this->setChartDataSets($chartDataSets);
        $this->setChartOptions($chartOptions);
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setLabels($labels)
    {
        $this->labels = $labels;
    }

    public function getLabels()
    {
        return $this->labels;
    }

    public function addLabel($label)
    {
        $this->labels[] = $label;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string chart id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param IChartDataSet[] $chartDataSets
     */
    public function setChartDataSets(array $chartDataSets)
    {
        $this->chartDataSets = $chartDataSets;
    }

    /**
     * @return IChartDataSet[]
     */
    public function getChartDataSets()
    {
        return $this->chartDataSets;
    }

    /**
     * @param IChartOptions $chartOptions
     */
    public function setChartOptions(IChartOptions $chartOptions)
    {
        $this->chartOptions = $chartOptions;
    }

    /**
     * @return IChartOptions
     */
    public function getChartOptions()
    {
        return $this->chartOptions;
    }

    /**
     * @param IChartDataSet $dataSet
     */
    public function addChartDataSet(IChartDataSet $dataSet)
    {
        $this->chartDataSets[$dataSet->getLabel()] = $dataSet;
    }

    public function getProperties()
    {
        return $this->properties;
    }

    public function setProperties($properties)
    {
        foreach($properties as $k => $v) {
            $this->setProperty($k, $v);
        }
    }

    public function getProperty($key)
    {
        return (isset($this->properties[$key]) ? $this->properties[$key] : null);
    }

    public function setProperty($key, $value)
    {
        $this->properties[$key] = $value;
    }

    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'labels' => $this->getLabels(),
            'props' => $this->getProperties(),
            'chartDataSets' => array_map(function($n) { return $n->toArray(); }, $this->getChartDataSets()),
            'chartOptions' => $this->getChartOptions()->toArray(),
        );
    }

} 