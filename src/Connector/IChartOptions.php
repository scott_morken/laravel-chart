<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 1:08 PM
 */

namespace Smorken\Chart\Connector;


interface IChartOptions {

    public function setOption($key, $value);

    public function setOptions($options);

    public function getOption($key);

    public function getOptions();

    public function toArray();
} 