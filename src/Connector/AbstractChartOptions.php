<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 1:06 PM
 */

namespace Smorken\Chart\Connector;


abstract class AbstractChartOptions {

    protected $options = array();

    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    public function setOption($key, $value)
    {
        $this->options[$key] = $value;
    }

    public function setOptions($options)
    {
        foreach($options as $k => $v) {
            $this->setOption($k, $v);
        }
    }

    public function getOption($key)
    {
        return (isset($this->options[$key]) ? $this->options[$key] : null);
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function toArray()
    {
        return $this->options;
    }
} 