<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 3:21 PM
 */

namespace Smorken\Chart\Connector\Chartjs;


use Smorken\Chart\Connector\AbstractChartOptions;
use Smorken\Chart\Connector\IChartOptions;

class ChartOptions extends AbstractChartOptions implements IChartOptions {

    protected $options = array(
        'legendTemplate' => "<ul class=\"legend <%=name.toLowerCase()%>-legend\"><% for (var i in datasets){%><li><span style=\"background-color:<%=datasets[i].fillColor%>;border: 1px solid <%=datasets[i].strokeColor%>;\"><%if(datasets[i].label){%><%=datasets[i].label%><%}%></span></li><%}%></ul>"
    );
} 