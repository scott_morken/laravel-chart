<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 3:19 PM
 */

namespace Smorken\Chart\Connector\Chartjs\Pie;


use Smorken\Chart\Connector\AbstractChart;
use Smorken\Chart\Connector\IChart;

class Chart extends AbstractChart implements IChart {

    protected $type = 'pie';

    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'type' => $this->getType(),
            'data' => array_map(function($d) { return $d->toArray(); }, $this->getChartDataSets()),
            'options' => $this->getChartOptions()->toArray(),
        );
    }
} 