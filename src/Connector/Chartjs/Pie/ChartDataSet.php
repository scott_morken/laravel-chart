<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 3:20 PM
 */

namespace Smorken\Chart\Connector\Chartjs\Pie;


use Smorken\Chart\Connector\AbstractChartDataSet;
use Smorken\Chart\Connector\IChartDataSet;

class ChartDataSet extends AbstractChartDataSet implements IChartDataSet {

    protected $properties = array(
        'color' => null,
        'highlight' => null,
    );

    public function setColors($r, $g, $b)
    {
        $c = array('color' => 1, 'highlight' => .8);
        foreach ($c as $which => $val) {
            $this->setColor($which, $r, $g, $b, $val);
        }
    }

    public function toArray()
    {
        $arr = array(
            'label' => $this->getLabel(),
            'value' => (isset($this->data['value']) ? $this->data['value'] : 0)
        );
        foreach($this->getProperties() as $k => $v) {
            if (!isset($arr[$k])) {
                $arr[$k] = $v;
            }
        }
        return $arr;
    }
}