<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 3:19 PM
 */

namespace Smorken\Chart\Connector\Chartjs;


use Smorken\Chart\Connector\AbstractChart;
use Smorken\Chart\Connector\IChart;

class Chart extends AbstractChart implements IChart {

    public function toArray()
    {
        $data = array(
            'labels' => $this->getLabels(),
            'datasets' => array_map(function($d) { return $d->toArray(); }, $this->getChartDataSets()),
        );
        return array(
            'id' => $this->getId(),
            'data' => $data,
            'options' => $this->getChartOptions()->toArray(),
        );
    }
} 