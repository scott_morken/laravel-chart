<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 1:07 PM
 */

namespace Smorken\Chart\Connector;


interface IChart {

    /**
     * @param string $id
     */
    public function setId($id);

    /**
     * @return string chart id
     */
    public function getId();

    public function setType($type);

    public function getType();

    public function setLabels($label);

    public function getLabels();

    public function addLabel($label);

    /**
     * @param IChartDataSet $chartDataSet
     */
    public function addChartDataSet(IChartDataSet $chartDataSet);

    /**
     * @return IChartDataSet[]
     */
    public function getChartDataSets();

    /**
     * @param IChartDataSet[] $chartDataSets
     */
    public function setChartDataSets(array $chartDataSets);

    /**
     * @param IChartOptions $chartOptions
     */
    public function setChartOptions(IChartOptions $chartOptions);

    /**
     * @return IChartOptions
     */
    public function getChartOptions();

    public function getProperties();

    public function setProperties($properties);

    public function getProperty($key);

    public function setProperty($key, $value);

    public function toArray();
} 