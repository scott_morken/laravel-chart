<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 1:05 PM
 */

namespace Smorken\Chart\Connector;


abstract class AbstractChartDataSet {

    protected $label;
    protected $data = array();
    protected $properties = array();

    public function __construct($label, $data, $properties = null)
    {
        $this->setLabel($label);
        $this->setData($data);
        if ($properties !== null) {
            $this->setProperties($properties);
        }
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

    public function addData($data)
    {
        $this->data[] = $data;
    }

    public function getProperties()
    {
        return $this->properties;
    }

    public function setProperties($properties)
    {
        foreach($properties as $k => $v) {
            $this->setProperty($k, $v);
        }
    }

    public function getProperty($key)
    {
        return (isset($this->properties[$key]) ? $this->properties[$key] : null);
    }

    public function setProperty($key, $value)
    {
        $this->properties[$key] = $value;
    }

    abstract public function setColors($r, $g, $b);

    public function setColor($property, $r, $g, $b, $a)
    {
        $this->setProperty($property, sprintf("rgba(%d,%d,%d,%s)", $r, $g, $b, $a));
    }

    public function toArray()
    {
        return array(
            'label' => $this->label,
            'data' => $this->data,
            'properties' => $this->properties,
        );
    }
} 