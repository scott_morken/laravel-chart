<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/30/14
 * Time: 12:12 PM
 */

namespace Smorken\Chart\Connector\Highcharts;


use Smorken\Chart\Connector\AbstractChartOptions;
use Smorken\Chart\Connector\IChartOptions;

class ChartOptions extends AbstractChartOptions implements IChartOptions {

    protected $convert = array('title', 'subtitle');

    public function toArray()
    {
        $ret = array();
        foreach($this->getOptions() as $key => $value) {
            if (in_array($key, $this->convert)) {
                continue;
            }
            $ret[$key] = $value;
        }
        $ret = $this->addConverted($ret);
        return $ret;
    }

    protected function addConverted($ret)
    {
        foreach($this->convert as $key) {
            $v = $this->getOption($key);
            if ($v) {
                $ret[$key]['text'] = $v;
            }
        }
        return $ret;
    }
} 