<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/30/14
 * Time: 12:12 PM
 */

namespace Smorken\Chart\Connector\Highcharts;


use Smorken\Chart\Connector\AbstractChartDataSet;
use Smorken\Chart\Connector\IChartDataSet;

class ChartDataSet extends AbstractChartDataSet implements IChartDataSet {

    public function setColors($r, $g, $b)
    {
        return null;
    }

    public function toArray()
    {
        $data = array(
            'name' => $this->getLabel(),
            'data' => $this->getData(),
        );
        if ($this->getProperty('stack')) {
            $data['stack'] = $this->getProperty('stack');
        }
        if ($this->getProperty('type')) {
            $data['type'] = $this->getProperty('type');
        }
        return $data;
    }
}