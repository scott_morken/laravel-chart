<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 3:19 PM
 */

namespace Smorken\Chart\Connector\Highcharts\Pie;


use Smorken\Chart\Connector\AbstractChart;
use Smorken\Chart\Connector\IChart;

class Chart extends AbstractChart implements IChart {

    protected $type = 'pie';

    public function toArray()
    {
        $data = array(
            'series' => [
                [
                    'type' => $this->getType(),
                    'data' => array_map(function($d) { return $d->toArray(); }, $this->getChartDataSets())
                ]
            ],
        );
        if ($this->getProperty('title')) {
            $data['title'] = array('text' => $this->getProperty('title'));
        }
        foreach($this->getChartOptions()->toArray() as $k => $v) {
            $data[$k] = $v;
        }
        return array(
            'id' => $this->getId(),
            'options' => $data,
        );
    }
} 