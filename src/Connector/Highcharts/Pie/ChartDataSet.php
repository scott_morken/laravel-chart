<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 3:20 PM
 */

namespace Smorken\Chart\Connector\Highcharts\Pie;


use Smorken\Chart\Connector\AbstractChartDataSet;
use Smorken\Chart\Connector\IChartDataSet;

class ChartDataSet extends AbstractChartDataSet implements IChartDataSet {

    public function setColors($r, $g, $b)
    {
        return null;
    }

    public function toArray()
    {
        $data = [
            'name' => $this->getLabel(),
            'y' => $this->getData()[0],
        ];
        if ($this->getProperty('type')) {
            $data['type'] = $this->getProperty('type');
        }
        return $data;
    }
}