<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 3:21 PM
 */

namespace Smorken\Chart\Connector\Highcharts\Pie;


use Smorken\Chart\Connector\Highcharts\ChartOptions as HighChartOptions;
use Smorken\Chart\Connector\IChartOptions;

class ChartOptions extends HighChartOptions implements IChartOptions {

}