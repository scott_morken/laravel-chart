<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/30/14
 * Time: 12:11 PM
 */

namespace Smorken\Chart\Connector\Highcharts;


use Smorken\Chart\Connector\AbstractChart;
use Smorken\Chart\Connector\IChart;

class Chart extends AbstractChart implements IChart {

    protected $type = 'column';

    public function toArray()
    {
        $data = array(
            'chart' => array(
                'type' => $this->getType(),
            ),
            'xAxis' => array(
                'categories' => $this->getLabels(),
            ),
            'series' => array_map(function($d) { return $d->toArray(); }, $this->getChartDataSets()),
        );
        if ($this->getProperty('title')) {
            $data['title'] = array('text' => $this->getProperty('title'));
        }
        if ($this->getProperty('yAxis')) {
            $data['yAxis'] = array('text' => $this->getProperty('yAxis'));
        }
        foreach($this->getChartOptions()->toArray() as $k => $v) {
            $data[$k] = $v;
        }
        return array(
            'id' => $this->getId(),
            'options' => $data,
        );
    }
} 