<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 10:06 AM
 */

namespace Smorken\Chart;


use Illuminate\Config\Repository;
use Illuminate\Support\Str;
use Illuminate\View\Factory;
use Illuminate\Support\Facades\Log;
use Smorken\Chart\Connector\ChartException;
use Smorken\Chart\Connector\IChart;
use Illuminate\Support\Facades\URL;
use Smorken\Chart\Storage\Asset;

class ChartHandler
{

    protected $preloaded = false;
    /**
     * @var array
     */
    protected $config = array();
    /**
     * @var \Illuminate\View\Factory
     */
    protected $view;
    /**
     * @var Asset
     */
    protected $assetProvider;
    protected $assets = array();
    protected $charts = array();
    protected $classes = array();

    public function __construct(Asset $assetProvider, Factory $view, array $config)
    {
        $this->assetProvider = $assetProvider;
        $this->view = $view;
        $this->config = $config;
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function setConfigOption($key, $value)
    {
        array_set($this->config, $key, $value);
    }

    public function addHtml($view_data)
    {
        $type = $this->getType();
        $view_defaults = array_get($this->config, $type . '.view_defaults', array());
        $view_data = array_merge($view_defaults, $view_data);
        return $this->getView('chart', $view_data);
    }

    public function getView($view, $view_data)
    {
        $type = $this->getType();
        $view_base = array_get($this->config, $type . '.view_base', null);

        if (!$view_base) {
            throw new ChartException("Unable to locate a view base for $type.");
        }
        return $this->view->make($view_base . '.' . $view, $view_data);
    }

    public function getType()
    {
        return array_get($this->config, 'type', null);
    }

    public function addChart(IChart $chart)
    {
        $this->charts[$chart->getId()] = $chart;
    }

    public function getAssetHeadFromProvider()
    {
        $html = array();
        $html[] = sprintf('<script type="text/javascript" src="%s?%s"></script>', route('chart.assets.js'), $this->assetProvider->getLastModified('js'));
        $html[] = sprintf('<link rel="stylesheet" href="%s?%s"></script>', route('chart.assets.css'), $this->assetProvider->getLastModified('css'));
        return implode("\n", $html);
    }

    public function generateJavascript()
    {
        $view_data = array('charts' => $this->getCharts());
        return $this->getView('js', $view_data);
    }

    public function generateChartJavascript()
    {
        $view_data = array('charts' => $this->getCharts());
        return $this->view->make('smorken/chart::init', $view_data);
    }

    public function hasCharts()
    {
        return count($this->charts) > 0;
    }

    public function getCharts()
    {
        return $this->charts;
    }

    public function hasAssets()
    {
        return count($this->assets) > 0;
    }

    public function getAssets()
    {
        return $this->assets;
    }

    public function preLoad()
    {
        if (!$this->preloaded) {
            $preloads = array_get($this->config, 'preload', array());
            foreach ($preloads as $path => $load) {
                if ($load) {
                    $this->generateAssetLink($path);
                }
            }
        }
        $this->preloaded = true;
    }

    /**
     * @param $request
     * @param \Symfony\Component\HttpFoundation\Response $response
     * Borrowed from Barryvdh/LaravelDebugBar
     * @return string
     */
    public function modifyResponse($request, $response)
    {
        $this->preLoad();
        $prepend = join("\n", $this->assets);
        $prepend .= $this->getAssetHeadFromProvider();
        $js = $this->generateJavascript();
        $content = $response->getContent();
        $pos = strripos($content, '</body>');
        if ($pos !== false) {
            $content = substr($content, 0, $pos) . $prepend . $js . substr($content, $pos);
        }
        elseif (($response->headers->has('Content-Type') &&
            strpos($response->headers->get('Content-Type'), 'html') !== false)
        ) {
            $content = $content . $prepend . $js;
        }
        $response->setContent($content);
        return $response;
    }

    public function generateAssetLink($path)
    {
        $link = URL::asset($path);
        $asset = $this->generateAsset($link);
        if ($asset) {
            $this->assets[] = $asset;
        } else {
            Log::warning("Could not create an asset from $path.");
        }
    }

    public function generateAsset($path)
    {
        $parts = pathinfo($path);
        $ext = $parts['extension'];
        $asset = '';
        switch ($ext) {
            case 'js':
                $asset = $this->generateJsAsset($path);
                break;
            case 'css':
                $asset = $this->generateCssAsset($path);
                break;
            default:
                throw new ChartException("Unexpected asset type.");
                break;
        }
        return $asset;
    }

    public function generateJsAsset($path)
    {
        return sprintf('<script src="%s"></script>', $path);
    }

    public function generateCssAsset($path)
    {
        return sprintf('<link href="%s" type="text/css" rel="stylesheet">', $path);
    }

    public function getChartClasses($ctype)
    {
        $type = $this->getType();
        if (isset($this->classes[$type][$ctype])) {
            return $this->classes[$type][$ctype];
        }
        $classes = array(
            'Chart' => null,
            'ChartDataSet' => null,
            'ChartOptions' => null,
        );
        $ctype = studly_case($ctype);
        $type = $this->getType();
        $base = array_get($this->config, $type . '.class_base', null);

        foreach($classes as $c => $n) {
            $classname = Str::finish($base, '\\') . Str::finish($ctype, "\\") . $c;
            if (class_exists($classname)) {
                $classes[$c] = $classname;
            }
            else {
                $classname = Str::finish($base, "\\") . $c;
                if (class_exists($classname)) {
                    $classes[$c] = $classname;
                }
            }
        }
        $this->classes[$type][$ctype] = $classes;
        return $classes;
    }
} 