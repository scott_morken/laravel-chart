<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 8:16 AM
 */

namespace Smorken\Chart\Helpers;


use Illuminate\Support\Collection;
use Smorken\Chart\Reports\Contracts\Model;

/**
 * Class ExampleChartBuilder
 * @package Smorken\Chart\Helpers
 *
 * Example using a collection built with the Smorken\Report\Manual library
 */
class ExampleChartBuilder extends BaseChartByData
{

}