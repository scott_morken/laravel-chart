<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 11:44 AM
 */

namespace Smorken\Chart\Helpers;

use Illuminate\Support\Str;

abstract class BaseChartByData {

    /**
     * @var Charter
     */
    protected $charter;

    public function __construct(Charter $charter)
    {
        $this->charter = $charter;
    }

    public function run($data, $type = 'bar', $value = 'avg')
    {
        $allowed_values = ['avg', 'sum', 'count'];
        if (!in_array($value, $allowed_values)) {
            throw new \InvalidArgumentException("$value is not an allowed aggregate function");
        }
        $charts = [];
        foreach($data as $chart_id => $sub_data) {
            $charts[] = $this->addChart($chart_id, $sub_data, $type, $value);
        }
        return $charts;
    }

    protected function addChart($chart_id, $external_data, $type = 'bar', $value = 'avg')
    {
        $chart_data = [
            'datasets' => [],
            'labels' => [],
            'options' => [],
        ];
        $chart_data['options']['title'] = $this->getChartTitle($external_data, $type, $value);
        $datasets = $this->getDataSets($external_data);
        $dataset_ids = array_keys($datasets);
        $default_series = $this->createDefaultSeries($dataset_ids);
        foreach($datasets as $dataset_id => $dataset) {
            $this->addDataSetLabel($dataset_id, $this->getDataSetLabel($dataset_id, $dataset), $chart_data);
            $this->iterateSeries($dataset_id, $this->getDataSet($dataset_id, $dataset), $chart_data, $default_series, $value);
        }
        return $this->charter->create($this->getChartId($chart_id, $external_data, $type, $value), $type, $chart_data);
    }

    protected function getChartId($chart_id, $data, $type = 'bar', $value = 'avg')
    {
        $parts = [
            $type,
            $value,
            Str::slug($this->getChartTitle($data)),
            $chart_id,
        ];
        return implode('-', $parts);
    }

    protected function getChartTitle($external_data, $type = 'bar', $value = 'avg')
    {
        $helpers = [
            'avg' => 'Average',
            'count' => 'Count',
            'sum' => 'Sum',
        ];
        $addtext = (isset($helpers[$value]) ? ' (' . $helpers[$value] . ')' : '');
        return array_get($external_data, 'name', null) . $addtext;
    }

    protected function getDataSets($external_data)
    {
        return array_get($external_data, 'data', array());
    }

    protected function addDataSetLabel($id, $label, &$chart_data)
    {
        if (!isset($chart_data['labels'][$id])) {
            $chart_data['labels'][$id] = $label;
        }
    }

    protected function getDataSetLabel($id, $dataset)
    {
        return array_get($dataset, 'name', $id);
    }

    protected function getDataSet($dataset_id, $dataset)
    {
        return array_get($dataset, 'data', array());
    }

    protected function createDefaultSeries($keys)
    {
        $defaults = ['data' => [], 'options' => [], 'name' => null];
        foreach($keys as $key) {
            $defaults['data'][$key] = 0;
        }
        return $defaults;
    }

    protected function iterateSeries($dataset_id, $series, &$chart_data, $default_series, $value)
    {
        foreach($series as $series_id => $data) {
            if (!isset($chart_data['datasets'][$series_id])) {
                $chart_data['datasets'][$series_id] = $default_series;
            }
            $chart_data['datasets'][$series_id]['data'][$dataset_id] = $data->$value();
            $chart_data['datasets'][$series_id]['name'] = $data->getLabel();
        }
    }
}