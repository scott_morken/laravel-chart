<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 8:09 AM
 */

namespace Smorken\Chart\Helpers;

use Smorken\Chart\Connector\IChart;
use Smorken\Chart\Connector\IChartDataSet;
use Smorken\Chart\Connector\IChartOptions;
use Smorken\Chart\Util\Color;

class Charter {

    /**
     * @var array chart classes to build
     */
    protected $classes = array();

    /**
     * @param string $type chart type to get classes for
     * @return array
     */
    protected function getChartClasses($type)
    {
        $ch = app('smorken.chart');
        $this->classes = $ch->getChartClasses($type);
        return $this->classes;
    }

    /**
     * @param string $chartid
     * @param string $type
     * @param array $results
     * @param array $options
     * @return IChart
     */
    public function create($chartid, $type, $results = array(), $options = array())
    {
        $this->getChartClasses($type);
        $ds = $this->createDataSets($results['datasets']);
        $chartlabels = array_values($results['labels']);
        if (!$options && isset($results['options'])) {
            $options = $results['options'];
        }
        $cclass = $this->classes['Chart'];
        return new $cclass($chartid, $chartlabels, $ds, $this->createOptions($options));
    }

    /**
     * Creates an instance of IChartOptions to pass into IChart
     * @param array $options
     * @return IChartOptions
     */
    protected function createOptions($options = array())
    {
        $oclass = $this->classes['ChartOptions'];
        return new $oclass($options);
    }

    /**
     * Creates an array of IChartDataSets
     * Expects an array of series label => series data array
     * $series data array should contain
     * data => data array
     * options => options array
     * [name] optional => name of series, uses series label if not set
     * @param array $results
     * @return IChartDataSet[]
     */
    protected function createDataSets($results)
    {
        $dsclass = $this->classes['ChartDataSet'];
        $datasets = array();
        foreach($results as $sl => $series) {
            $s = array_get($series, 'data', $series);
            $o = array_get($series, 'options', array());
            $sl = array_get($series, 'name', $sl);
            if (!is_array($s)) {
                $s = (array) $s;
            }
            $ds = new $dsclass($sl, array_values($s), $o);
            list($r, $g, $b) = Color::get($sl);
            $ds->setColors($r, $g, $b);
            $datasets[] = $ds;
        }
        return $datasets;
    }
}