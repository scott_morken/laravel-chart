<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/18/15
 * Time: 9:53 AM
 */

namespace Smorken\Chart\Controllers;

use Illuminate\Http\Response;
use Smorken\Chart\Storage\Asset;

/**
 * Class AssetController
 * @package Smorken\Chart\Controllers
 *
 * Many ideas are borrowed from the awesome barryvdh/laravel-debugbar
 * @link https://github.com/barryvdh/laravel-debugbar
 */
class AssetController extends BaseController {

    /**
     * @var Asset
     */
    protected $provider;

    /**
     * @param Asset $assetProvider
     */
    public function __construct(Asset $assetProvider)
    {
        $this->provider = $assetProvider;
    }

    public function js()
    {
        $env = $this->getEnvironment();
        $assets = $this->provider->all('js', $env);
        $content = $this->provider->render($assets);
        $response = new Response(
            $content, 200, array(
                'Content-Type' => 'text/javascript',
            )
        );
        return $this->cacheResponse($response);
    }

    public function css()
    {
        $env = $this->getEnvironment();
        $assets = $this->provider->all('css', $env);
        $content = $this->provider->render($assets);
        $response = new Response(
            $content, 200, array(
                'Content-Type' => 'text/css',
            )
        );
        return $this->cacheResponse($response);
    }

    protected function getEnvironment()
    {
        return \App::environment('local', 'development', 'staging') ? 'dev': 'prod';
    }

    /**
     * Cache the response 1 year (31536000 sec)
     */
    protected function cacheResponse(Response $response)
    {
        $response->setSharedMaxAge(31536000);
        $response->setMaxAge(31536000);
        $response->setExpires(new \DateTime('+1 year'));
        return $response;
    }
}