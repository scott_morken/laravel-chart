<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/18/15
 * Time: 9:54 AM
 */

namespace Smorken\Chart\Controllers;

use Smorken\Chart\ChartHandler;
use Illuminate\Routing\Controller;

class BaseController extends Controller {

    /**
     * @var ChartHandler
     */
    protected $chartHandler;

    public function __construct(ChartHandler $chartHandler)
    {
        $this->chartHandler = $chartHandler;
    }
}