<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 10:34 AM
 */

namespace Smorken\Chart\Util;


class Color {

    const MIN_AVG = 64;
    const MAX_AVG = 192;
    const STEP = 16;

    public static function get($str)
    {
        $range = Color::MAX_AVG - Color::MIN_AVG;
        $factor = $range / 256;
        $offset = Color::MIN_AVG;

        $base_hash = substr(md5($str), 0, 6);
        $b_R = hexdec(substr($base_hash,0,2));
        $b_G = hexdec(substr($base_hash,2,2));
        $b_B = hexdec(substr($base_hash,4,2));

        $f_R = floor((floor($b_R * $factor) + $offset) / Color::STEP) * Color::STEP;
        $f_G = floor((floor($b_G * $factor) + $offset) / Color::STEP) * Color::STEP;
        $f_B = floor((floor($b_B * $factor) + $offset) / Color::STEP) * Color::STEP;

        return array($f_R, $f_G, $f_B);
    }

    public static function random($length = 20)
    {
        $start = 43;
        $end = 126;
        $str = '';
        for ($i = 0; $i < $length; $i ++) {
            $str .= chr(rand($start, $end));
        }
        return $str;
    }
}