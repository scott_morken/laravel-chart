<?php namespace Smorken\Chart;

use Illuminate\Support\ServiceProvider;

class ChartServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
        $this->loadViewsFrom(__DIR__ . '/../views', 'smorken/chart');
//        $this->publishes([
//            __DIR__ . '/../bower_components' => base_path() . '/public/js',
//            __DIR__ . '/../resources/assets/js' => base_path() . '/public/js',
//        ], 'js');
        $this->publishes([
            __DIR__ . '/../views' => base_path() . '/resources/views/vendor/smorken/chart',
        ], 'views');
        $this->publishes([
            __DIR__ . '/../config' => config_path() . '/vendor/smorken/chart',
        ], 'config');

        $this->addRoutes();

        $ch = $this->app['smorken.chart'];
        $app = $this->app;
        $self = $this;
        $app['router']->after(
            function($request, $response) use ($self, $ch) {
                if (!$request->ajax() && $self->isLoadNeeded($request, $ch)) {
                    $ch->modifyResponse($request, $response);
                }
            }
        );
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->registerResources();
        $this->app->bindShared('smorken.chart', function($app) {
            $asset = $app['Smorken\Chart\Storage\Asset'];
            $config = $app['config']->get('smorken/chart::config');
            return new ChartHandler($asset, $app['view'], $config);
        });
        $this->app->bind('Smorken\Chart\Storage\Asset', function($app) {
            return new \Smorken\Chart\Storage\FileAsset($app['files'], __DIR__ . '/../');
        });
	}

    protected function registerResources()
    {
        $files = array('config');
        foreach($files as $file) {
            $userconfigfile = sprintf("%s/vendor/smorken/chart/%s.php", config_path(), $file);
            $packageconfigfile = sprintf("%s/../config/%s.php", __DIR__, $file);
            $this->registerConfig($packageconfigfile, $userconfigfile, 'smorken/chart::' . $file);
        }
    }

    protected function registerConfig($packagefile, $userfile, $namespace)
    {
        $config = $this->app['files']->getRequire($packagefile);
        if (file_exists($userfile)) {
            $userconfig = $this->app['files']->getRequire($userfile);
            $config = array_replace_recursive($config, $userconfig);
        }
        $this->app['config']->set($namespace, $config);
    }

    protected function addRoutes()
    {
        $routeConfig = [
            'namespace' => 'Smorken\Chart\Controllers',
            'prefix' => '_chart',
        ];
        $this->app['router']->group($routeConfig, function($router) {
            $router->get('assets/css', [
                'uses' => 'AssetController@css',
                'as' => 'chart.assets.css',
            ]);
            $router->get('assets/js', [
                'uses' => 'AssetController@js',
                'as' => 'chart.assets.js',
            ]);
        });
    }

    protected function isLoadNeeded($request, $ch)
    {
        if ($ch->hasCharts()) {
            return true;
        }
        $route = $request->route();
        if ($route) {
            $action = $route->getAction();
            return ($action && isset($action['chart']) && $action['chart']);
        }
        return false;
    }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('smorken.chart');
	}

}
