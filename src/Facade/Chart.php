<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 10:06 AM
 */

namespace Smorken\Chart\Facade;

use Illuminate\Support\Facades\Facade as F;

class Chart extends F {

    protected static function getFacadeAccessor() { return 'smorken.chart'; }

} 