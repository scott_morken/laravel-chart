<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/18/15
 * Time: 10:13 AM
 */

namespace Smorken\Chart\Storage;


interface Asset {

    public function find($id);

    public function getLastModified($type);

    public function all($type, $environment = 'prod');

    public function render($asset_ids);

}