<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/18/15
 * Time: 10:13 AM
 */

namespace Smorken\Chart\Storage;


use Illuminate\Filesystem\Filesystem;

class FileAsset implements Asset {

    /**
     * @var Filesystem
     */
    protected $backend;

    protected $basePath;

    public function __construct(Filesystem $filesystem, $basePath = './')
    {
        $this->backend = $filesystem;
        $this->basePath = $basePath;
    }

    public function load($id)
    {
        $id = $this->getPath($id);
        if ($this->backend->exists($id)) {
            return $this->backend->get($id);
        }
    }

    protected function getPath($id)
    {
        return $this->basePath . $id;
    }

    public function find($id)
    {
        $id = $this->getPath($id);
        if ($this->backend->exists($id)) {
            return $this->backend->get($id);
        }
    }

    public function getLastModified($type)
    {
        $assets = $this->all($type, 'prod');
        $latest = 0;
        foreach($assets as $asset) {
            $mtime = filemtime($this->getPath($asset));
            if ($mtime > $latest) {
                $latest = $mtime;
            }
        }
        return $latest;
    }

    public function all($type, $environment = 'prod')
    {
        $charttype = config('smorken/chart::config.type', 'chartjs');
        $chartoptions = config('smorken/chart::config.' . $charttype, array());
        $typeoptions = array_get($chartoptions, $type, array());
        return array_get($typeoptions, $environment, array());
    }

    public function render($asset_ids)
    {
        $output = array();
        foreach($asset_ids as $id) {
            $content = $this->find($id);
            if ($content) {
                $output[] = $content;
            }
        }
        return implode("\n", $output);
    }
}