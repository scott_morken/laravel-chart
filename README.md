* Add to your composer.json file for the Laravel project

* Initialize all the components from the vendor/smorken/chart directory

     * bower install

 * add the service provider to your `config/app.php`

 ```
 'providers' = [
     ...
     'Smorken\Chart\ChartServiceProvider',
     ...
 ],
 ```

* add the package assets to the project

    * `php artisan vendor:publish --provider="Smorken\Chart\ChartServiceProvider"`

* you can force the chart assets to load by adding `'chart' => true` to a route

* the chart assets should automatically be loaded if a chart is added

Setup Code

```
//initialize the chart handler
$chartlabels = array('my label 1', 'my label 2');
$chartoptions = array('of some chart options');
$mydatasets = array('some label' => array(0, 1.5)); //array corresponds to each of the chartlabels
$ch = \App::make('smorken.chart');
$classes = $ch->getChartClasses('column');
$datasets = array();
$datasetclass = $classes['ChartDataSet'];
foreach($mydatasets as $series_label => $series) {
  $series = array_values ($series);
  $options = array();
  $ds = new $datasetclass($series_label, $series, $options);
  //list($r, $g, $b) = Color::get($series_label);
  //$ds->setColors($r, $g, $b);
  $datasets[] = $ds;
}
$optionclass = $classes['ChartOptions'];
$options = new $optionclass($chartoptions);
$chartclass = $classes['Chart'];
$chart_id = 'mychart-' . $some_id;
$chart = new $chartclass($chart_id, $chartlabels, $datasets, $options);
```

View

```
{!! Chart::addHtml(array(
'id' => $chart->getId(),
'class' => 'chart',
'height' => '400px',
'width' => '100%',
)) !!}
<?php Chart::addChart($chart); ?>
```

## Using Smorken\Report\Manual library

Example data

```
<?php
$data = [
    1 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 1,
        'question' => ['name' => 'Question 1'],
        'value' => 1
    ],
    2 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 2,
        'question' => ['name' => 'Question 2'],
        'value' => 2
    ],
    3 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 3,
        'question' => ['name' => 'Question 3'],
        'value' => 3
    ],
    4 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 1,
        'question' => ['name' => 'Question 1'],
        'value' => 1
    ],
    5 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 2,
        'question' => ['name' => 'Question 2'],
        'value' => 2
    ],
    6 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 3,
        'question' => ['name' => 'Question 3'],
        'value' => 3
    ],
    7 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 1,
        'question' => ['name' => 'Question 1'],
        'value' => 1
    ],
    8 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 2,
        'question' => ['name' => 'Question 2'],
        'value' => 2
    ],
    9 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 3,
        'question' => ['name' => 'Question 3'],
        'value' => 3
    ],
];
```

Result handler

```
<?php

use Smorken\Report\Manual\Results\Result;
use Smorken\Report\Manual\Contracts\Model;

class MyResult extends Result {

    protected $types = [
        'report_type' => [
            'voclass' => 'Smorken\Report\Manual\Models\VO',
            'value_callback' => 'valueCallbackOne',
            'key_callback' => 'keyCallbackOne',
            'vo_callback' => 'voCallbackOne',
        ],
        'chart_type' => [
            'voclass' => 'Smorken\Report\Manual\Models\VO',
            'value_callback' => 'valueCallbackTwo',
            'key_callback' => 'keyCallbackTwo',
            'vo_callback' => 'voCallbackTwo',
        ],
    ];


     public function valueCallbackOne($key, $data)
     {
        $value = $data['value'];
        if ($value !== null && $value == floatval($value)) {
            return $value;
        }
        return false;
     }

     public function keyCallbackOne($voclass, $key, $data)
     {
        return $data['question_id'];
     }

     public function voCallbackOne($key, $data, Model $model)
     {
        $model->setData($data['question']);
        $model->setLabel($data['question']['name']);
         return $model;
     }


    public function valueCallbackTwo($key, $data)
    {
        $value = $data['value'];
        if ($value !== null && $value == floatval($value)) {
            return $value;
        }
        return false;
    }

    public function keyCallbackTwo($voclass, $key, $data, &$results)
    {
        $survey_id = $data['survey_id'];
        $q_id = $data['question_id'];
        if (!isset($results[$survey_id])) {
            $results[$survey_id] = [
                'name' => $data['survey']['name'],
                'data' => [],
            ];
        }
        if (!isset($results[$survey_id]['data'][$q_id])) {
            $results[$survey_id]['data'][$q_id] = new $voclass;
        }
        return $results[$survey_id]['data'][$q_id];
    }

    public function voCallbackTwo($key, $data, Model $model)
    {
        $model->setData($data['question']);
        $model->setLabel($data['question']['name']);
        return $model;
    }
}
```

```
<?php

use Smorken\Chart\Helpers\BaseChartByData

class ChartSurveys extends BaseChartByData {

}
```
Use

```
$items = new \Smorken\Report\Manual\Items\Iterable($data);
$handler = new \Smorken\Report\Manual\Handlers\IterableHandler($items);
$resultBuilder = new MyResult($handler);
$results = $resultBuilder->run('custom_type');
$chartBuilder = new ChartSurveys($charter);
$charts = $chartBuilder->run($results/*, 'bar', 'avg'*/);
```
