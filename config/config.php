<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/8/14
 * Time: 10:04 AM
 */
return array(
    'chartjs' => array(
        'view_base' => 'smorken/chart::chartjs',
        'class_base' => "\\Smorken\\Chart\\Connector\\Chartjs\\",
        'view_defaults' => array(
            'height' => 300,
            'width' => 400,
        ),
        'js' => array(
            'dev' => array(
                '/bower_components/chartjs/Chart.js',
            ),
            'prod' => array(
                '/bower_components/chartjs/Chart.min.js',
            ),
        ),
    ),
    'highcharts' => array(
        'view_base' => 'smorken/chart::highcharts',
        'class_base' => "\\Smorken\\Chart\\Connector\\Highcharts\\",
        'view_defaults' => array(
            'height' => 300,
            'width' => 400,
        ),
        'js' => array(
            'dev' => array(
                '/bower_components/highcharts-release/highcharts.src.js',
                '/plugins/grouped-categories.highcharts.js',
            ),
            'prod' => array(
                '/bower_components/highcharts-release/highcharts.js',
                '/resources/assets/js/plugins/grouped-categories.highcharts.js',
            ),
        ),
    ),
    'type' => 'chartjs',
);