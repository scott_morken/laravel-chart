<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 12:05 PM
 */

include __DIR__ . '/MyResult.php';

use Smorken\Report\Manual as srm;

class IntegrationTest extends PHPUnit_Framework_TestCase {

    public function testGetResults()
    {
        $results = $this->getResults();
        //print_r($results);
        foreach($results['status']['data']['status']['data'] as $i => $vo) {
            $this->assertEquals($vo->sum(), $vo->avg() * 2);
        }
    }

    public function testGetCharts()
    {
        include __DIR__ . '/ChartByResultAndValue.php';
        include __DIR__ . '/Charter.php';
        $results = $this->getResults();
        $charter = new Charter();
        $builder = new ChartByResultAndValue($charter);
        $charts = $builder->run($results, 'pie', 'avg');
        //print_r($charts[0]->toArray());
        $results = $charts[0]->toArray();
        $this->assertEquals('pie-avg-status-average-status', $results['id']);
        $this->assertCount(4, $results['options']['series']);
    }

    protected function getResults()
    {
        $data = include __DIR__ . '/data.php';
        $items = new srm\Items\Iterable($data);
        $handler = new srm\Handlers\IterableHandler($items);
        $builder = new MyResult($handler);
        $results = $builder->run('kill_or_not');
        return $results;
    }
}