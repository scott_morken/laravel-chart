<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 12:03 PM
 */

use Smorken\Report\Manual\Results\Result;
use Smorken\Report\Manual\Contracts\Model;

class MyResult extends Result {

    protected $types = [
        'kill_or_not' => [
            'voclass' => 'Smorken\Report\Manual\Models\VO',
            'value_callback' => 'valueCallbackTwo',
            'key_callback' => 'keyCallbackTwo',
        ],
    ];

    protected function setupKillVOs($voclass, $keys, &$results)
    {
        if (!$results) {
            $results['status'] = [
                'name' => 'Status',
                'data' => [
                    'status' => [
                        'name' => 'Status',
                        'data' => [],
                    ]
                ],
            ];
        }
        foreach($keys as $k => $name) {
            if (!isset($results['status']['data']['status']['data'][$k])) {
                $vo = new $voclass;
                $vo->setLabel($name);
                $results['status']['data']['status']['data'][$k] = $vo;
            }
        }
    }

    public function valueCallbackTwo($key, $data)
    {
        $value = $data['loc'];
        if ($value !== null && $value == floatval($value)) {
            return $value;
        }
        return false;
    }

    public function keyCallbackTwo($voclass, $key, $data, &$results)
    {
        $keys = ['kill' => 'Needs EOL', 'eol' => 'EOL', 'inactive' => 'Inactive', 'active' => 'Active'];
        $this->setupKillVOs($voclass, $keys, $results);
        $kill = $data['kill'];
        $active = $data['active'];
        $currkey = null;
        if ($kill && $active) {
            $currkey = 'kill';
        }
        elseif ($kill && !$active) {
            $currkey = 'eol';
        }
        elseif (!$kill && $active) {
            $currkey = 'active';
        }
        else {
            $currkey = 'inactive';
        }
        if ($currkey) {
            return $results['status']['data']['status']['data'][$currkey];
        }
    }

}