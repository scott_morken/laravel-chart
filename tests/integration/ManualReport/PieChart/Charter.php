<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/4/15
 * Time: 5:52 PM
 */



class Charter extends \Smorken\Chart\Helpers\Charter {

    /**
     * @param string $type chart type to get classes for
     * @return array
     */
    protected function getChartClasses($type)
    {
        $this->classes = [
            'Chart' => 'Smorken\Chart\Connector\Highcharts\Pie\Chart',
            'ChartDataSet' => 'Smorken\Chart\Connector\Highcharts\Pie\ChartDataSet',
            'ChartOptions' => 'Smorken\Chart\Connector\Highcharts\Pie\ChartOptions',
        ];
        return $this->classes;
    }

}