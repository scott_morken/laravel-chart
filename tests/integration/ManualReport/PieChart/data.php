<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 12:03 PM
 */
return [
    1 => [
        'project_id' => 1,
        'project' => ['name' => 'Project 1'],
        'kill' => 1,
        'active' => 1,
        'loc' => 10
    ],
    2 => [
        'project_id' => 2,
        'project' => ['name' => 'Project 2'],
        'kill' => 0,
        'active' => 1,
        'loc' => 20
    ],
    3 => [
        'project_id' => 3,
        'project' => ['name' => 'Project 3'],
        'kill' => 1,
        'active' => 0,
        'loc' => 30
    ],
    4 => [
        'project_id' => 4,
        'project' => ['name' => 'Project 4'],
        'kill' => 0,
        'active' => 0,
        'loc' => 40
    ],
    5 => [
        'project_id' => 5,
        'project' => ['name' => 'Project 5'],
        'kill' => 1,
        'active' => 1,
        'loc' => 10
    ],
    6 => [
        'project_id' => 6,
        'project' => ['name' => 'Project 6'],
        'kill' => 0,
        'active' => 1,
        'loc' => 20
    ],
    7 => [
        'project_id' => 7,
        'project' => ['name' => 'Project 7'],
        'kill' => 1,
        'active' => 0,
        'loc' => 30
    ],
    8 => [
        'project_id' => 8,
        'project' => ['name' => 'Project 8'],
        'kill' => 0,
        'active' => 0,
        'loc' => 40
    ],
];