<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/9/14
 * Time: 11:22 AM
 */

use Smorken\Chart\Connector\Dummy\Chart;
use Mockery as m;

class ChartTest extends PHPUnit_Framework_TestCase {

    /**
     * @var Chart
     */
    protected $sut;

    public function setUp()
    {
        $this->dataSets = $this->mockDataSets();
        $this->options = $this->mockOptions();
        $labels = $this->getLabels();
        $this->sut = new Chart('foo-id', $labels, $this->dataSets, $this->options);
    }

    public function teardown()
    {
        m::close();
    }

    public function testGetId()
    {
        $this->assertEquals('foo-id', $this->sut->getId());
    }

    public function testSetId()
    {
        $this->sut->setId('bar-id');
        $this->assertEquals('bar-id', $this->sut->getId());
    }

    public function testAddDataChartSet()
    {
        $ds = $this->mockDataSet();
        $ds->shouldReceive('getLabel')->andReturn('foo-label');
        $this->sut->addChartDataSet($ds);
        $sets = $this->sut->getChartDataSets();
        $this->assertCount(3, $sets);
        $this->assertArrayHasKey('foo-label', $sets);
    }

    public function testSetChartDataSets()
    {
        $this->sut->setChartDataSets(array());
        $this->assertCount(0, $this->sut->getChartDataSets());
    }

    public function testGetChartOptions()
    {
        $this->assertInstanceOf('\Smorken\Chart\Connector\IChartOptions', $this->sut->getChartOptions());
    }

    public function testGetProperties()
    {
        $this->assertEquals(array(), $this->sut->getProperties());
    }

    public function testSetProperties()
    {
        $this->sut->setProperties(array('foo' => 'bar'));
        $this->assertEquals(array('foo' => 'bar'), $this->sut->getProperties());
    }

    public function testSetProperty()
    {
        $this->sut->setProperty('foo', 'bar');
        $this->assertEquals(array('foo' => 'bar'), $this->sut->getProperties());
    }

    public function testGetProperty()
    {
        $this->sut->setProperty('foo', 'bar');
        $this->assertEquals('bar', $this->sut->getProperty('foo'));
    }

    public function testToArray()
    {
        $expected = array(
            'id' => 'foo-id',
            'props' => array(),
            'chartDataSets' => array(
                'l1' => array(
                    'label' => 'l1',
                    'data' => array(),
                ),
                'l2' => array(
                    'label' => 'l2',
                    'data' => array(),
                )
            ),
            'chartOptions' => array(),
            'labels' => array('l1', 'l2'),
        );
        foreach($this->dataSets as $i => $ds) {
            $ds->shouldReceive('toArray')->andReturn(array('label' => $i, 'data' => array()));
        }
        $this->options->shouldReceive('toArray')->andReturn(array());
        $this->assertEquals($expected, $this->sut->toArray());
    }

    protected function mockDataSets()
    {
        return array(
            'l1' => $this->mockDataSet(),
            'l2' => $this->mockDataSet(),
        );
    }

    protected function mockOptions()
    {
        return m::mock('\Smorken\Chart\Connector\IChartOptions');
    }

    protected function mockDataSet()
    {
        $m = m::mock('\Smorken\Chart\Connector\IChartDataSet');
        return $m;
    }

    protected function getLabels()
    {
        return array('l1', 'l2');
    }
} 