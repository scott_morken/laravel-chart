<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/9/14
 * Time: 11:56 AM
 */

use Smorken\Chart\Connector\Dummy\ChartOptions;

class ChartOptionsTest extends PHPUnit_Framework_TestCase {

    /**
     * @var ChartOptions
     */
    protected $sut;

    public function setUp()
    {
        $this->sut = new ChartOptions();
    }

    public function testConstructorWithOptions()
    {
        $sut = new ChartOptions(array('foo' => 'bar'));
        $this->assertEquals(array('foo' => 'bar', 'fiz' => 'bat'), $sut->getOptions());
    }

    public function testToArray()
    {
        $this->assertEquals(array('fiz' => 'bat'), $this->sut->toArray());
    }
} 