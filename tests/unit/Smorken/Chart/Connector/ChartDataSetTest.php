<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/9/14
 * Time: 11:40 AM
 */

use Smorken\Chart\Connector\Dummy\ChartDataSet;

class ChartDataSetTest extends PHPUnit_Framework_TestCase {

    /**
     * @var ChartDataSet
     */
    protected $sut;

    public function setUp()
    {
        $this->sut = new ChartDataSet('foo-label', array('foo' => 'bar'));
    }

    public function testGetLabel()
    {
        $this->assertEquals('foo-label', $this->sut->getLabel());
    }

    public function testSetLabel()
    {
        $this->sut->setLabel('bar-label');
        $this->assertEquals('bar-label', $this->sut->getLabel());
    }

    public function testGetData()
    {
        $this->assertEquals(array('foo' => 'bar'), $this->sut->getData());
    }

    public function testSetData()
    {
        $this->sut->setData(array('biz' => 'baz'));
        $this->assertEquals(array('biz' => 'baz'), $this->sut->getData());
    }

    public function testAddData()
    {
        $this->sut->addData('bar');
        $this->assertEquals(array('foo' => 'bar', 'bar'), $this->sut->getData());
    }

    public function testGetProperties()
    {
        $this->assertTrue(is_array($this->sut->getProperties()));
    }

    public function testSetProperties()
    {
        $count = count($this->sut->getProperties());
        $props = array('biz' => 'bat');
        $this->sut->setProperties($props);
        $this->assertCount($count + 1, $this->sut->getProperties());
    }

    public function testSetProperty()
    {
        $this->sut->setProperty('foo', 'bar');
        $this->assertEquals('bar', $this->sut->getProperty('foo'));
    }

    public function testSetColor()
    {
        $this->sut->setColor('foo', 1, 2, 3, 4);
        $this->assertEquals('rgba(1,2,3,4)', $this->sut->getProperty('foo'));
    }

    public function testSetColors()
    {
        $this->sut->setColors(100, 100, 100);
        $this->assertEquals('rgba(100,100,100,0.5)', $this->sut->getProperty('fillColor'));
    }

    public function testToArray()
    {
        $arr = $this->sut->toArray();
        $this->assertEquals('foo-label', $arr['label']);
        $this->assertArrayHasKey('data', $arr);
        $this->assertArrayHasKey('properties', $arr);
    }
} 