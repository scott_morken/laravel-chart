<?php
use Smorken\Chart\Util\Color;

/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/9/14
 * Time: 11:11 AM
 */


class ColorTest extends \PHPUnit_Framework_TestCase {

    public function testRandom()
    {
        $r = Color::random();
        $this->assertEquals(20, strlen($r));
    }

    public function testRandomLength()
    {
        $r = Color::random(43);
        $this->assertEquals(43, strlen($r));
    }

    public function testRandomIsDifferent()
    {
        $rs = array();
        for ($i = 0; $i < 10; $i ++) {
            $rs[] = Color::random();
        }
        foreach($rs as $i => $v) {
            unset($rs[$i]);
            $this->assertNotContains($v, $rs);
        }
    }

    public function testColor()
    {
        $rgb = Color::get('foo');
        $this->assertEquals(array(144, 144, 64), $rgb);
    }

    public function testColorRandomIsDifferent()
    {
        $rgb1 = Color::get(Color::random());
        $rgb2 = Color::get(Color::random());
        $this->assertNotEquals($rgb1, $rgb2);
    }
} 