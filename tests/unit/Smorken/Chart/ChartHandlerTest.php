<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/9/14
 * Time: 7:45 AM
 */

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Smorken\Chart\ChartHandler;
use Mockery as m;



class ChartHandlerTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \Smorken\Chart\ChartHandler
     */
    protected $sut;

    public function setUp()
    {
        $this->view = m::mock('\Illuminate\View\Factory');
        $this->sut = new ChartHandler($this->view, $this->getConfig());
    }

    public function tearDown()
    {
        m::close();
    }

    public function testGetType()
    {
        $this->assertEquals('foo', $this->sut->getType());
    }

    public function testGetView()
    {
        $this->mockGetView();
        $this->assertEquals('view html', $this->sut->getView('bar', array()));
    }

    public function testAddHtml()
    {
        $view_data = array('foo' => 'bar');
        $this->mockGetView('chart', $view_data);
        $this->assertEquals('view html', $this->sut->addHtml($view_data));
    }

    public function testAddChartExpectationFails()
    {
        $chart = 'fiz';
        $this->setExpectedException('\PHPUnit_Framework_Error');
        $this->sut->addChart($chart);
    }

    public function testAddChart()
    {
        $chart = m::mock('\Smorken\Chart\Connector\IChart');
        $chart->shouldReceive('getId')
            ->andReturn('foo-chart');
        $this->sut->addChart($chart);
        $this->assertTrue($this->sut->hasCharts());
    }

    public function testHasChartsFalseWhenEmpty()
    {
        $this->assertFalse($this->sut->hasCharts());
    }

    public function testGenerateAssetFromJs()
    {
        $js = 'path/to/asset.js';
        $this->assertEquals('<script src="path/to/asset.js"></script>', $this->sut->generateAsset($js));
    }

    public function testGenerateAssetFromCss()
    {
        $css = 'path/to/asset.css';
        $this->assertEquals('<link href="path/to/asset.css" type="text/css" rel="stylesheet">', $this->sut->generateAsset($css));
    }

    public function testGenerateAssetUnknownTypeThrowsException()
    {
        $path = '/path/to/asset.ext';
        $this->setExpectedException('\Smorken\Chart\Connector\ChartException');
        $this->sut->generateAsset($path);
    }

    public function testPreLoad()
    {
        $this->mockUrlAsset();
        $this->sut->preLoad();
        $this->assertTrue($this->sut->hasAssets());
        $this->assertEquals(2, count($this->sut->getAssets()));
    }

    public function testGenerateJavascriptNoCharts()
    {
        $this->mockGetView('js', array('charts' => array()));
        $this->assertEquals('view html', $this->sut->generateJavascript());
    }

    public function testGenerateJavascriptWithCharts()
    {
        $chart = m::mock('\Smorken\Chart\Connector\IChart');
        $chart->shouldReceive('getId')
            ->andReturn('foo-chart');
        $this->sut->addChart($chart);
        $this->mockGetView('js', array('charts' => $this->sut->getCharts()));
        $this->assertEquals('view html', $this->sut->generateJavascript());
    }

    public function testModifyResponse()
    {
        $expected = 'original content.<script src="path/to/asset.js"></script>
<link href="path/to/asset.css" type="text/css" rel="stylesheet">view html</body>';
        //preload
        $this->mockUrlAsset();
        //generateJavascript
        $this->mockGetView('js', array('charts' => array()));

        $response = m::mock('\Symfony\Component\HttpFoundation\Response');
        $response->shouldReceive('getContent')
            ->andReturn('original content.</body>', $expected);
        $response->shouldReceive('setContent')
            ->with($expected);
        $this->assertEquals($expected, $this->sut->modifyResponse(null, $response)->getContent());
    }

    public function testGetChartClasses()
    {
        $expected = array(
            'Chart' => '\Smorken\Chart\Connector\Dummy\Chart',
            'ChartDataSet' => '\Smorken\Chart\Connector\Dummy\Line\ChartDataSet',
            'ChartOptions' => '\Smorken\Chart\Connector\Dummy\ChartOptions',
        );
        $ctype = 'Line';
        $this->sut->setConfigOption('type', 'dummy');
        $this->assertEquals($expected, $this->sut->getChartClasses($ctype));
    }

    protected function mockUrlAsset()
    {
        $p = array_keys($this->getPreloadAssets(), true);
        URL::shouldReceive('asset')->andReturnValues($p);
    }

    protected function mockGetView($view_name = 'bar', $view_data = array())
    {
        $this->view->shouldReceive('make')
            ->with('baz.' . $view_name, $view_data)
            ->andReturn('view html');
    }

    protected function getPreloadAssets()
    {
        return array(
            'path/to/asset.js' => true,
            'path/to/asset.css' => true,
            'path/to/noload.js' => false,
        );
    }

    protected function getConfig()
    {
        return array(
            'type' => 'foo',
            'foo' => array(
                'view_base' => 'baz',
                'view_defaults' => array(),
            ),
            'dummy' => array(
                'class_base' => '\\Smorken\\Chart\\Connector\\Dummy\\'
            ),
            'preload' => $this->getPreloadAssets(),
        );
    }
}