<script>
    var smorken = smorken || {};

    smorken.chart = {
        init: function() {

        },
        createChart: function(chartObj) {
            var id = chartObj['id'];
            var options = chartObj['options'];
            smorken.chart.preprocessSeries(options.series);
            $('#' + id).highcharts(options);
        },
        preprocessSeries: function(series) {
            $.each(series, function(i, v) {
                $.each(v['data'], function(j, w) {
                    if (typeof w === 'object') {
                        return true;
                    }
                    v['data'][j] = parseFloat(w);
                });
            });
        }
    };

    $(document).ready(function() {
        smorken.chart.init();
    });
</script>
@include('smorken/chart::init')