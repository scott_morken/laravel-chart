@if ($charts)
    <script>
        $(document).ready(function() {
            @foreach($charts as $chart)
            smorken.chart.createChart({!! json_encode($chart->toArray()) !!});
            @endforeach
        });
    </script>
@endif