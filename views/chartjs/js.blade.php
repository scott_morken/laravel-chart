<script>
    var smorken = smorken || {};

    smorken.chart = {
        charts: {},
        init: function() {

        },
        createChart: function(chartObj) {
            var self = smorken.chart;
            var id = chartObj['id'];
            var type = chartObj['type'] || 'Bar';
            type = type.charAt(0).toUpperCase() + type.slice(1);
            var data = chartObj['data'];
            var options = chartObj['options'];
            if (id in self.charts && self.charts[id]) {
                self.charts[id].destroy();
            }
            var c = smorken.chart.getChart(id);
            self.charts[name] = c[type](data, options);
            if (options['legendTemplate']) {
                var legend = self.charts[name].generateLegend();
                $('#' + id).parent().append(legend);
            }
        },
        getChart: function(id) {
            var ctx = $('#' + id).get(0).getContext('2d');
            return new Chart(ctx);
        }
    };

    $(document).ready(function() {
        smorken.chart.init();
    });
</script>
@include('smorken/chart::init')